#include "../../include/EX1/EX1.h"

#include <stdio.h>
#include <unistd.h>

enum ReturnCodes { SUCCESS = 0, FAILURE = 1 };

enum YesNo { Yes = 1, No = 0 };

static enum YesNo getAnswer(void) {
    for (;;) {
        switch (fgetc(stdin)) {
        case 'y':
        case 'Y':
            return Yes;
        case 'n':
        case 'N':
            return No;
        default:
            break;
        }
    }
}

int main(int argc, const char *argv[]) {
    /* Check argument count */
    if (2 != argc) {
        fprintf(stderr, "Invalid parameters.\n%s [output file path]\n",
                argv[0]);
        return FAILURE;
    }

    /* Check for existing file */
    if (access(argv[1], F_OK) == 0) {
        printf("File '%s' exists. Overwrite? [y/n]\n", argv[1]);
        if (getAnswer() == No) {
            return SUCCESS;
        }
    }

    printf("Writing to file . . .");
    fflush(stdout);
    ex1(argv[1]);
    puts(" [OK]\n");

    puts("End of execution");
    return SUCCESS;
}
