#include "../../include/EX1/EX1.h"

#include <fcntl.h>
#include <unistd.h>

static const char spam_char = 0;

void ex1(const char *filename) {
    const int output_fd = open(filename, O_WRONLY | O_TRUNC | O_CREAT, 0666);

    for (size_t i = 0; i < FILE_SIZE; ++i) {
        write(output_fd, &spam_char, 1);
    }

    close(output_fd);
}
