#ifndef TEN_MB_H
#define TEN_MB_H

/* 10 MB */
#define FILE_SIZE 10000000

/**
 * Write FILE_SIZE bytes of data to file 'filename'.
 * @param filename the file name.
 */
void ex1(const char *filename);

#endif
