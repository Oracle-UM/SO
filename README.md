# SO

## [EN]

Operating Systems [2018/2019] solved exercises<br/>

__Several changes were made to the original scripts.<br/>
Check each `Changes.md` in the script's respective directory to see which.__

---

## [PT]

Sistemas Operativos [2018/2019] exercícios resolvidos<br/>

__Foram feitas várias alterações ao guiões originais.<br/>
Verifica cada `Changes.md` na diretoria do guião respetivo para ver quais.__
