#include "../../include/Person/Person.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* imposed on input */
#define MAX_AGE_DIGITS 3
#define MAX_NAME_LENGTH 100

enum YesNo { Yes = 1, No = 0 };

enum ErrCodes { ErrInvArgc = 1, ErrInvArgv = 2, ErrNullName = 3 };

/* Reach the end of stdin */
static inline void _seek_stdin_end(void) {
    while ('\n' != fgetc(stdin)) {
        continue;
    }
}

static enum YesNo getAnswer(void) {
    for (;;) {
        switch (fgetc(stdin)) {
        case 'y':
        case 'Y':
            _seek_stdin_end();
            return Yes;
        case 'n':
        case 'N':
            _seek_stdin_end();
            return No;
        default:
            break;
        }
    }
}

static Age strtoage(const char *buff) {
    errno = 0;
    char *endptr;
    const int age = strtoul(buff, &endptr, 10);
    if (buff == endptr || '\0' != *endptr || age < 0) {
        errno = EINVAL;
    }
    return (Age)age;
}

int main(int argc, const char *argv[]) {
    /* Check argument count */
    if (3 != argc) {
        fprintf(stderr, "Invalid parameters.\n%s [person name] [person age]\n",
                argv[0]);
        return ErrInvArgc;
    }

    /* Parse main input */
    const char *person_name = argv[1];
    const Age person_age = strtoage(argv[2]);
    if (EINVAL == errno || ERANGE == errno) {
        fprintf(stderr, "Invalid age value.\n");
        return ErrInvArgv;
    }

    /* Create original person */
    printf("Creating new person . . .");
    Person *person = personConstruct(person_name, person_age);
    if (!person) {
        puts(" [ERROR: NULL name]\nAborting execution.");
        return ErrNullName;
    } else {
        puts(" [OK]\n");
    }

    /* Print original person */
    puts("Printing created person:");
    personPrint(person);

    /* Copy original person */
    printf("Creating a copy of the original person . . .");
    Person *person_copy = personCopy(person);
    if (!person_copy) {
        puts(" [ERROR: NULL name]\nAborting execution.");
        return ErrNullName;
    } else {
        puts(" [OK]\n");
    }

    /* Print copy */
    puts("Printing the copy of the original person:");
    personPrint(person_copy);

    /* Destruct original person */
    printf("Destructing original person . . .");
    personDestruct(&person);
    puts(" [OK]\n");

    /* Change copy name */
    puts("Do you want to modify the copied person's name? [y/n]");
    if (getAnswer() == Yes) {
        char new_name[MAX_NAME_LENGTH + 1];
        printf("New name? [max. %d characters]\n", MAX_NAME_LENGTH);
        fgets(new_name, MAX_NAME_LENGTH + 1, stdin);
        new_name[strcspn(new_name, "\r\n")] = '\0';

        printf("Changing copied person name to %s . . .", new_name);
        personChangeName(person_copy, new_name);
        puts(" [OK]\n");
    }

    /* Change copy age */
    puts("Do you want to modify the copied person's age? [y/n]");
    if (getAnswer() == Yes) {
        char new_age_buff[MAX_AGE_DIGITS + 1];
        printf("New age? [max. %d digits]\n", MAX_AGE_DIGITS);
        fgets(new_age_buff, MAX_AGE_DIGITS + 1, stdin);
        new_age_buff[strcspn(new_age_buff, "\r\n")] = '\0';

        Age new_age = strtoage(new_age_buff);
        if (EINVAL == errno || ERANGE == errno) {
            puts("Invalid age value, not changing age.");
        } else {
            printf("Changing copied person age to %u . . .", new_age);
            personChangeAge(person_copy, new_age);
            puts(" [OK]\n");
        }
    }

    /* Print copy */
    puts("\nPrinting copied person again:");
    personPrint(person_copy);

    /* Destruct copy */
    printf("Destructing copied person . . .");
    personDestruct(&person_copy);
    puts(" [OK]\n");

    puts("End of execution.");
    return EXIT_SUCCESS;
}
