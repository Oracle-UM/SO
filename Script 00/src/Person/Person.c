#include "../../include/Person/Person.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Person {
    char *name;
    Age age;
};

Person *personConstruct(const char *name, Age age) {
    if (!name) {
        return NULL;
    }

    Person *new_person = malloc(sizeof(Person));
    new_person->name = strdup(name);
    new_person->age = age;

    return new_person;
}

Person *personCopy(const Person *person) {
    return personConstruct(person->name, person->age);
}

char *personGetName(const Person *person) { return strdup(person->name); }

Age personGetAge(const Person *person) { return person->age; }

int personChangeName(Person *person, const char *name) {
    if (!name) {
        return EXIT_FAILURE;
    }

    free(person->name);
    person->name = strdup(name);
    return EXIT_SUCCESS;
}

void personChangeAge(Person *person, Age age) { person->age = age; }

void personPrint(const Person *person) {
    const Age age = personGetAge(person);
    char *name = personGetName(person);
    printf("{\n\tName: %s\n\tAge: %u\n}\n\n", name, age);
    free(name);
}

void personDestruct(Person **person) {
    free((*person)->name);
    free(*person);
    *person = NULL;
}
