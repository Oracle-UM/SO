# Changes made from the original script

## [EN]
- Methods that directly used `Person`s instead use pointers to `Person`s. This was done to provide implementation abstraction to the structure `Person`;
- `Person` methods have the prefix 'person', have been renamed more consistently, and use camelCase instead of snake_case:
    - `new_person` -> `personConstruct`
    - `clone_person` -> `personCopy`
    - `destroy_person` -> `personDestruct`
    - `person_age` -> `personGetAge`
    - `person_change_age` -> `personChangeAge`
- Method `personDestruct` not only deallocates the `Person`s name, but the whole structure itself;
- Method `personGetName` was implemented to complement method `personGetAge`, and method `personPrint` was implemented to facilitate testing;
- `const` qualifiers were added to method arguments that aren't modified by said methods ([const correctness](https://isocpp.org/wiki/faq/const-correctness));
- A `main` function demonstrating the API was implemented.
- A `Doxyfile` and documentation were added. Use `make docs` to generate it.

## [PT]
- Métodos que usavam diretamente `Person`s passaram a usar apontadores para `Person`s. Isto foi alterado para proporcionar abstração da implementação da estrutura de dados `Person`;
- Métodos de `Person`s têm o prefixo 'person', foram renomeados de uma forma mais consistente, e usam camelCase em vez de snake_case:
    - `new_person` -> `personConstruct`
    - `clone_person` -> `personCopy`
    - `destroy_person` -> `personDestruct`
    - `person_age` -> `personGetAge`
    - `person_change_age` -> `personChangeAge`
- O método `personDestruct` não só desaloca o espaço ocupado pelo nome da `Person`, mas também a própria estrutura na sua totalidade;
- O método `personGetName` foi implementado para complementar o método `personGetAge`, e o método `personPrint` foi implementado para facilitar a execução de testes;
- Qualificadores `const` foram adicionados aos argumentos de métodos que não são alterados por estes mesmos métodos ([const correctness](https://isocpp.org/wiki/faq/const-correctness));
- Uma função `main` que demonstra a API foi implementada.
- Um `Doxyfile` e documentação foram adicionados. Usa `make docs` para a gerar.
