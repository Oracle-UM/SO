#ifndef PERSON_H
#define PERSON_H

typedef unsigned Age;

typedef struct Person Person;

/**
 * Construct a Person with a name and age.
 * @param name the Person's name.
 * @param age the Person's age.
 * @return On success, a pointer holding the Person is returned;
 * On failure (i. e., NULL name), NULL is returned.
 */
Person *personConstruct(const char *name, Age age);

/**
 * Create a copy of a Person.
 * @param person the address of the Person to be copied.
 * @return On success, a pointer holding a copy is returned;
 * On failure (i. e., NULL name), NULL is returned.
 */
Person *personCopy(const Person *person) /* const */;

/**
 * Return a copy of a Person's name.
 * @param person the address of the Person.
 * @return the Person's name.
 */
char *personGetName(const Person *person) /* const */;

/**
 * Return a Person's age.
 * @param person the address of the Person.
 * @return the Person's age.
 */
Age personGetAge(const Person *person) /* const */;

/**
 * Format print a Person to stdout.
 * @param person the address of the Person.
 */
void personPrint(const Person *person) /* const */;

/**
 * Change a Person's name.
 * @param person the address of the Person.
 * @param new_name the Person's new name.
 * @return On success, EXIT_SUCCESS is returned;
 * On failure (i. e., NULL name), EXIT_FAILURE is returned.
 */
int personChangeName(Person *person, const char *new_name);

/**
 * Change a Person's age.
 * @param person the address of the Person.
 * @param new_age the Person's new age.
 */
void personChangeAge(Person *person, Age new_age);

/**
 * Deallocate all memory used by a *Person, and set it to NULL.
 * @param person the address of the Person.
 */
void personDestruct(Person **person);

#endif /* PERSON_H */
